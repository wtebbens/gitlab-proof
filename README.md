# Gitlab proof

[Verifying my OpenPGP key: openpgp4fpr:B1A1EFACC8B7E5609985393DF3D118E30E6AC770]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/
